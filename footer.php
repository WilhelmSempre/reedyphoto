<footer class="col-xs-12 no-padding">
    <section class="container">
        <nav class="pull-left col-xs-12 no-padding">
            <ul class="no-padding">
                <li><a rel="nofollow" class="text-uppercase scroll" href="/" data-target="">Strona głowna</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#offer" data-target=".offer">Oferta</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#portfolio" data-target=".portfolio">Portfolio</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#aboutus" data-target=".aboutus">O nas</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#contact" data-target=".contact">Kontakt</a></li>
            </ul>
        </nav>
        <p class="copyright text-center col-xs-12 no-padding">
            Reedy.photo &copy; Wszelkie prawa zastrzeżone, kopiowanie materiałów zawartych na stronie bez uprzedniej zgody właściciela jest zabronione.
        </p>
    </section>
</footer>
<div class="slideout-widget slideout-big widget-facebook-2">
    <div class="slideout-widget-handler">
        <div class="svg-wrapper">
            <svg class="ico">
                <use xlink:href="#shape-social" />
            </svg>
        </div>
    </div>
    <div class="slideout-widget-content">
        <div class="fb-page" data-href="https://www.facebook.com/reedy.fotografia/?fref=ts" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"></div></div>
    </div>
</div>
</main>
<script async src="assets/js/dist/main.min.js"></script>
</body>
</html>
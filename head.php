<?php
$request = array_map(function($item) {
    return htmlentities(trim(strip_tags($item)));
}, $_GET);

$page = isset($request['s']);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="ReedyPhoto - Fotografia - Produkcja materiałów wideo">
    <meta property="og:title" content="ReedyPhoto - Fotografia - Produkcja materiałów wideo">
    <meta property="og:description" content="ReedyPhoto to grupa młodych, kreatywnych osób, które zajmują się fotografią oraz tworzeniem materiałów wideo, spotów reklamowych, filmów szkoleniowych">
    <meta property="og:url" content="http://www.reedy.photo">
    <meta property="og:image" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <meta name="description" content="ReedyPhoto to grupa młodych, kreatywnych osób, które zajmują się fotografią oraz tworzeniem materiałów wideo, spotów reklamowych, filmów szkoleniowych">
    <meta name="keywords" content="reedy, reedyphoto, fotografia, wideofilmowanie, obrobka,graficzna, zdjec, fotografia slubna, artystyczny reportaż slubny, sesje narzeczeńskie i plenerowe, fotograf, zdjęcia slubne">
    <link rel="stylesheet" href="assets/css/dist/style.min.css">
    <title>ReedyPhoto - Fotografia - Produkcja materiałów wideo</title>
</head>
<body>
<div class="svg-defs"></div>
<header class="navigation no-padding col-xs-12">
    <section class="container-fluid">
        <a href="/" data-target="" class="pull-left logo scroll"><img alt="ReedyPhoto - Fotografia - Produkcja materiałów wideo" src="assets/img/logo.png"></a>
        <nav class="pull-right menu visible-lg visible-md">
            <ul class="pull-right no-padding no-margin list-unstyled">
                <li><a rel="nofollow" class="text-uppercase scroll" href="/" data-target="">Strona Główna</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#offer" data-target=".offer">Oferta</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#portfolio" data-target=".portfolio">Portfolio</a></li>
                <li><a rel="nofollow" class="text-uppercase scroll" href="/#contact" data-target=".contact">Kontakt</a></li>
            </ul>
        </nav>
    </section>
</header>
<section class="col-xs-12 no-margin no-padding carousel carousel-fade slide rotator" data-ride="carousel">
    <section class="carousel-inner" role="listbox">
        <aside class="item text-center active no-margin row no-padding col-xs-12">
            <img class="img-responsive" src="assets/img/rotator/1.jpg" alt="1">
        </aside>
        <aside class="item text-center no-margin row no-padding col-xs-12">
            <img class="img-responsive" src="assets/img/rotator/2.jpg" alt="2">
        </aside>
        <aside class="item text-center no-margin row no-padding col-xs-12">
            <img class="img-responsive" src="assets/img/rotator/3.jpg" alt="3">
        </aside>
        <aside class="item text-center no-margin row no-padding col-xs-12">
            <img class="img-responsive" src="assets/img/rotator/4.jpg" alt="4">
        </aside>
        <aside class="item text-center no-margin row no-padding col-xs-12">
            <img class="img-responsive" src="assets/img/rotator/5.jpg" alt="5">
        </aside>
    </section>
    <div class="controls">
        <img data-wow-delay="0.1s" class="img-responsive logo wow fadeInDown" src="assets/img/logo.png">
        <a data-wow-delay="0.2s" class="btn green client-panel text-uppercase wow fadeInDown" href="http://reedyphoto.pixieset.com" target="_blank">Przejdź do panelu klienta!</a>
        <a data-wow-delay="0.3s" class="btn gray hire text-uppercase scroll wow fadeInDown" href="/" data-target=".offer">Przejdź do strony!</a>
    </div>
</section>
<main class="col-xs-12 no-padding <?php echo ($page) ? 'page' : 'main'; ?>">
        
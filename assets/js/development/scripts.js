(function($) {

    "use strict";

    var reedy = reedy || {};

    reedy = {

        animate: function(navigation, navHeight) {
            var scrollPosition = $(window).scrollTop(),
                pinNavHeight = parseInt(navigation.css('height')),
                logo = navigation.find('.logo');

            if (scrollPosition >= 130) {
                if (navigation.is(':visible') && !navigation.is(':animated') && pinNavHeight == navHeight) {
                    navigation.animate({ height : 60, backgroundColor: '#000' }, 200);
                    logo.fadeIn(200);
                }
            } else {
                if (!navigation.is(':animated') && pinNavHeight == 60) {
                    navigation.animate({ height : navHeight, backgroundColor: 'transparent' }, 200);
                    logo.fadeOut(200);
                }
            }
        },

        pin: function () {
            var pinNavigation = $('header.navigation'),
                pinNavigationHeight = pinNavigation.outerHeight(false);

            reedy.animate(pinNavigation, pinNavigationHeight);

            $(window).on('scroll', function (event) {
                reedy.animate(pinNavigation, pinNavigationHeight);
            });
        },

        scroll: function () {
            var scroll = $('.scroll');

            scroll.on('click', function (event) {
                event = event || window.event;

                var address = location.search;

                if (address == "") {
                    if (event.preventDefault()) {
                        event.preventDefault();
                    }
                }

                var target = $(this).data('target');
                target = (target == '') ? 0 : target;

                $('body').scrollTo(target, 1000, {easing: 'linear'});

            });
        },

        portfolioCategory: function () {

            /* Isotope */
            var isotope = $('.isotope');

            if (isotope.length > 0) {

                var category = isotope.isotope({
                    itemSelector: '.item',
                    layoutMode: 'fitRows'
                });

                var categoryInput = $('.portfolio .category');

                categoryInput.find('a').on('click', function (event) {
                    event = event || window.event;

                    categoryInput.find('a').removeClass('active');

                    var categoryValue = $(this).attr('data-filter');
                    category.isotope({filter: categoryValue});


                    $(this).addClass('active');

                    if (event.preventDefault()) {
                        event.preventDefault();
                    }
                });
            }
        },

        rotator: function() {

            var rotator = $('.rotator');

            rotator.carousel({
                pause: 'false'
            });
        },

        init: function () {

            new WOW().init();

            reedy.portfolioCategory();
            reedy.scroll();
            reedy.pin();
            reedy.rotator();
        }
    };

    $(document).ready(function() {

        reedy.init();

        if (location.hash != '') {
            var page = '.' + location.hash.slice(1);
            $('body').scrollTo(page, 1000, {easing: 'linear'});
        }

        $('select').selectric();

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.5&appId=1018864111469528";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-74367882-1', 'auto');
        ga('send', 'pageview');

    });

})(jQuery);